//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_usart.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <delay.h>
#include <uart.h>
#include <ryb080.h>


typedef struct
{
  uint8_t AnswerWaiting         :1;     // ��� �������� ������ �� �������
  uint8_t AnswerReceived        :1;     // ����� �� ������� �������
  uint8_t LineWaiting           :1;     //
  uint8_t LineReceived          :1;     //
  uint8_t ErrorReceived         :1;     // ��� ������ �������
  uint8_t NeedWakeUp            :1;     // �������� ��������� ������ (����� ��������� ������� ����� ������ ���)
  uint8_t ErrorCode;                    // ��� ������, ������� ������ ������ � ����� �� ������
  char *pAnswerBuff;
  char AnswerWord[16];                  // ��������� ����� � ������
}
tLoraState;

#define WAKE_TRY_NUM            5       // ���-�� ������� ��������� ������ ������
#define PORT_NUM                2       // ���������� ����������� ������ ������

static LoraEventFunc EventFunc = 0;  // ��������� �� �������, ���������� �� ������� ������ (�������� �����) 
static tLoraState Module_State;
static eConnectionState portState[2];
static uint16_t MTU[2] = {0, 0};
static uint8_t LastConnectedPort = 0;
static char StrBuff[256];

//==============================================================================
// ������� �������� ������ �� UART (�� �������� ��������� ������)
//==============================================================================
static int8_t SendStr(char *pBuff)
{
  uint16_t Len = strlen(pBuff);
  return UART_Send((uint8_t *)pBuff, Len);
}
//==============================================================================


//==============================================================================
//
//==============================================================================
static void WakeUpIfNeeded(void)
{
  if (Module_State.NeedWakeUp)
  {
    uint8_t tryNum = WAKE_TRY_NUM;
    while (tryNum--)
    {
      if (!Reyax_bt_IsPresent())
          return;
      delay_ms(10);
    }
  }
}
//==============================================================================


//==============================================================================
// ��������������� �������-��������� ��� ������ ��������� ����������� 
//==============================================================================
static eConnectionState convertToConnectionStatus(char value)
{
  switch (value)
  {
  case 'C':
  case 'c':
    return client_connection;
  case 'H':
  case 'h':
    return host_connection;
  default:
    return no_connection;
  }
}
//==============================================================================


//==============================================================================
// ������� ����� ������ �� �������
//==============================================================================
static int8_t RecvAnswer(char *pAnswer, char *pRecvBuff, uint32_t TimeOut)
{
  Module_State.AnswerWaiting = 1;
  Module_State.pAnswerBuff = pRecvBuff;
  strcpy(Module_State.AnswerWord, pAnswer);
  
  while (TimeOut)
  {
    if (Module_State.ErrorReceived)         // ��� ������ ������
      return Module_State.ErrorCode;
    else if (Module_State.AnswerReceived)   // ����� ������
    {
      Module_State.AnswerReceived = 0;
      return UART_ERR_OK;
    }
    
    TimeOut--;
    delay_ms(1);
  }

  Module_State.AnswerWaiting = 0;
  Module_State.AnswerWord[0] = 0;
  return REYAX_ERR_NO_ANSVER;
}
//==============================================================================


//==============================================================================
// ������� ����� ������ �� �������
//==============================================================================
static int8_t RecvLine(char *pRecvBuff, uint32_t TimeOut)
{
  Module_State.LineWaiting = 1;
  Module_State.LineReceived = 0;
  Module_State.pAnswerBuff = pRecvBuff;
  
  while (TimeOut)
  {
    if (Module_State.ErrorReceived)     // ��� ������ ������
      return Module_State.ErrorCode;
    else if (Module_State.LineReceived) // ������ �������
      return UART_ERR_OK;
    
    TimeOut--;
    delay_ms(1);
  }

  Module_State.LineWaiting = 0;
  return REYAX_ERR_NO_ANSVER;
}
//==============================================================================



//==============================================================================
// ��������� ������� ��������� ������ �� ������
//==============================================================================
static void RxRFPacket(uint8_t portNum, char *pBuff, uint16_t len)
{
  // ���������� ������� ��������� �������?
  if (EventFunc)
  {
    pBuff[len] = 0;     // ������� ������� ����� ������ (� ������ ���������� ��� ���)
    EventFunc(portNum, pBuff);
  }
}
//==============================================================================

uint32_t conn_events = 0;
uint32_t disconn_events = 0;

static void connect_event(char *pBuff, uint16_t Len)
{
  if (Len >= 9)
  {
    uint8_t portNum = pBuff[6] - 0x31;
    if (portNum < PORT_NUM)
    {
      portState[portNum] = convertToConnectionStatus(pBuff[5]); 
      LastConnectedPort = portNum;
    }
  }
  conn_events++;
}

//==============================================================================
// �������-���������� ���� ������� �� ������ (���������� �� ����������� ����������!)
//==============================================================================
static void RxPacket(char *pBuff, uint16_t Len)
{
  // ������� ����� ����
  if (!Len)
    return;
  /*
  // ������ ������ ������ ����� ����� ������� (�������� ���������� � �����), ������� ������� ������ ������ � ������ 
  if (Module_State.RxLen + Len < RX_BUFF_LEN)
  {
    memcpy(&Module_State.RxBuff[Module_State.RxLen], pBuff, Len);
    Module_State.RxLen += Len;
  }
  char *pRxBuff = Module_State.RxBuff;
  uint8_t RxLen = Module_State.RxLen;
    */

  char *pRxBuff = pBuff;
  uint8_t RxLen = Len;

  // �������� ����� (�� BT)
  if (RxLen >= 2 && *pRxBuff > 0x30 && *pRxBuff <= (0x30 + PORT_NUM) && *(pRxBuff + 1) == ':')
  {
    uint8_t portNum = *pRxBuff - 0x30;
    pRxBuff += 2;
    RxLen -= 2;
    RxRFPacket(portNum, pRxBuff, RxLen);
  }
  else if (strstr(pRxBuff, "+++++") && (pRxBuff[RxLen - 2] == 0x0D) && (pRxBuff[RxLen - 1] == 0x0A))           // ������� ����������� � ������
  {
    connect_event(pRxBuff, RxLen);
  }
  else if (strstr(pRxBuff, "+-----") && (pRxBuff[RxLen - 2] == 0x0D) && (pRxBuff[RxLen - 1] == 0x0A))     // ������� ���������� �� ������
  {
    if (RxLen >= 10)
    {
      uint8_t portNum = pRxBuff[7] - 0x31;
      if (portNum < 2)
        portState[portNum] = no_connection; 
    }
    disconn_events++;
  }
  else if (strstr(pRxBuff, "+MTU:"))      // ������������ ����� ������������ � �������� ������
  {
    pRxBuff += 5;
    MTU[LastConnectedPort] = atoi(pRxBuff);
  }
  else if (Module_State.LineWaiting)            // ���� ������
  {
    if (RxLen > 3 && (pRxBuff[0] == '+') && (pRxBuff[RxLen - 2] == 0x0D) && (pRxBuff[RxLen - 1] == 0x0A))
    {    
      pRxBuff++;
      RxLen--;
      if (Module_State.pAnswerBuff)
      {
        memcpy(Module_State.pAnswerBuff, pRxBuff, RxLen);
        Module_State.pAnswerBuff[RxLen] = 0;
      }

      Module_State.LineWaiting = 0;
      Module_State.LineReceived = 1;
    }
  }
  else if (Module_State.AnswerWaiting)      // ����� �� ������
  {
    if (RxLen > 3 && (pRxBuff[0] == '+') && (pRxBuff[RxLen - 2] == 0x0D) && (pRxBuff[RxLen - 1] == 0x0A))
    {
      char *wordStart = strstr(pRxBuff, Module_State.AnswerWord);
      if (wordStart)
      {
        uint16_t step = wordStart - pRxBuff;
        pRxBuff += step;
        RxLen -= step;
      
        // ����� �������� �����, ������ ����� ����
        int8_t WordLen = strlen(Module_State.AnswerWord);
        pRxBuff += WordLen;
        RxLen -= WordLen;
      
        // ���� "=")
        if ((RxLen) && (*pRxBuff == '='))
        {
          pRxBuff++;
          RxLen--;
        
          // ���������� �����
          if (Module_State.pAnswerBuff)
          {
            memcpy(Module_State.pAnswerBuff, pRxBuff, RxLen);
            Module_State.pAnswerBuff[RxLen] = 0;
          }
        }

        Module_State.AnswerWaiting = 0;
        Module_State.AnswerReceived = 1;
      }
    }
  }
  else          // ����� �� ���������� ��������� (��������, �� ���������� ����������)
  {
    RxRFPacket(0, pRxBuff, RxLen);
  }
}
//==============================================================================


//==============================================================================
// ������� ���������� � ������ pStrBuff ����� ����� Value
//==============================================================================
static int8_t DecToStr(int32_t Value, char* pStrBuff)
{
  uint32_t Divider = 1000000000;
  int8_t Len = 0;
  int8_t Started = 0;
  
  if (Value == 0)  
  {
    *(pStrBuff++) = '0';
    Len++;
  }
  else
  {
    while (Divider)
    {
      // ������� ��������� �����
      uint8_t Digit = (Value / Divider) % 10;
    
      // ���� �� 0, �� ������� ����� � ������
      if ((Digit) || (Started))
      {
        *(pStrBuff++) = Digit + 0x30;
        Len++;
        Started = 1;
      }
    
      Divider /= 10;
    }
  }
  
  *pStrBuff = 0x00;
  return Len;
}
//==============================================================================

/*
//==============================================================================
// ������� ���������� � ������ pStrBuff ���� Value � ����������������� ����
//==============================================================================
static void ByteToHexStr(uint8_t Value, char* pStrBuff)
{
  int8_t Val = Value >> 4;
  
  if (Val < 10)
    *pStrBuff = 0x30 + Val;             // 0-9
  else
    *pStrBuff = 0x41 - 10 + Val;        // A-F
  
  pStrBuff++;
  
  Val = Value & 0x0F;
  
  if (Val < 10)
    *pStrBuff = 0x30 + Val;             // 0-9
  else
    *pStrBuff = 0x41 - 10 + Val;        // A-F
  
  pStrBuff++;
  
  *pStrBuff = 0x00;
}
//==============================================================================
*/

//==============================================================================
// ������� ������ �� ������ pStrBuff ���� Value � ����������������� ����
//==============================================================================
static void HexStrToByte(uint8_t *pValue, char* pStrBuff)
{
  int8_t Value = 0;
  int8_t Val = 0;
  
  if (*pStrBuff < 0x3A)
    Val = *pStrBuff - 0x30;             // 0-9
  else
    Val = *pStrBuff - 0x41 + 10;        // A-F
  
  Value = Val << 4;
  pStrBuff++;
  
  if (*pStrBuff < 0x3A)
    Val = *pStrBuff - 0x30;             // 0-9
  else
    Val = *pStrBuff - 0x41 + 10;        // A-F

  Value |= Val;
  *pValue = Value;
}
//==============================================================================


//==============================================================================
// ������� �������� ������� �������� ����� �� UART � �������
//==============================================================================
int8_t Reyax_bt_IsPresent()
{
  SendStr("AT\r\n");
  return RecvAnswer("OK", 0, 20);
}
//==============================================================================


//==============================================================================
// ������� �������� ������� ��������� ��������� ����� GPIO (���� 5-7)
//==============================================================================
int8_t Reyax_bt_GPIO_set(uint8_t pin_num, uint8_t state)
{
  if ((pin_num < 5) || (pin_num > 7))
    return REYAX_ERR_WRONG_PARAM;
    
  WakeUpIfNeeded();

  SendStr("AT+GPIO=");
  
  char Buff[3];

  // Pin number
  DecToStr(pin_num, Buff);
  SendStr(Buff);
  SendStr(",");
  // State
  DecToStr(state ? 1 : 0, Buff);
  SendStr(Buff);
  SendStr("\r\n");

  return RecvAnswer("OK", 0, 20);
}
//==============================================================================


//==============================================================================
// ������� �������� ������� ������ ������
//==============================================================================
int8_t Reyax_bt_Reset()
{
  WakeUpIfNeeded();

  SendStr("AT+RESET\r\n");
  return RecvAnswer("READY", 0, 500);
}
//==============================================================================


//==============================================================================
// ������� ������ ������ �������� ������
//==============================================================================
int8_t Reyax_bt_GetVersion(char *pVersion)
{
  WakeUpIfNeeded();

  SendStr("AT+CGMS?\r\n");  // ����� ����: +CGMS=RYB080I_56312E30
  
  char Buff[33];
  int8_t err = RecvAnswer("CGMS", Buff, 250);
  
  strcpy(pVersion, Buff);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ ������������������ �����
//==============================================================================
int8_t Reyax_bt_GetBroadcastName(char *pName)
{
  WakeUpIfNeeded();

  SendStr("AT+NAME?\r\n");  // ����� ����: +NAME= REYAX_BLE_RYB080I
  
  char Buff[33];
  int8_t err = RecvAnswer("NAME", Buff, 250);
  
  strcpy(pName, Buff);

  return err;
}
//==============================================================================

 
//==============================================================================
// ������� ������ ������������������ �����. ��� ���������� �������� ����� �����.
//==============================================================================
int8_t Reyax_bt_SetBroadcastName(char *pName)
{
  WakeUpIfNeeded();

  SendStr("AT+NAME=");
  SendStr(pName);
  SendStr("\r\n");
  
  return RecvAnswer("OK", 0, 250);
}
//==============================================================================
 
  
//==============================================================================
// ������� ������ ����� ���������� (������������ � ���������� ��� iOS)
//==============================================================================
int8_t Reyax_bt_GetDeviceName(char *pName)
{
  WakeUpIfNeeded();

  SendStr("AT+ATTR?\r\n");  // ����� ����: +ATTR= REYAX_BLE_RYB080I 
  
  char Buff[33];
  int8_t err = RecvAnswer("ATTR", Buff, 250);
  
  strcpy(pName, Buff);

  return err;
}
//==============================================================================

 
//==============================================================================
// ������� ������ ����� ���������� (�� 20 ��������, ������������ � ���������� ��� iOS). ��� ���������� �������� ����� �����.
//==============================================================================
int8_t Reyax_bt_SetDeviceName(char *pName)
{
  WakeUpIfNeeded();

  SendStr("AT+ATTR=");
  SendStr(pName);
  SendStr("\r\n");
  
  return RecvAnswer("OK", 0, 250);
}
//==============================================================================
 

//==============================================================================
// ������� ������ �������� �����-�����������
//==============================================================================
int8_t Reyax_bt_GetRfPower(eRfPower *pPower)
{
  WakeUpIfNeeded();

  SendStr("AT+CRFOP?\r\n");  // ����� ����: +CRFOP=4
  
  char Buff[33];
  int8_t err = RecvAnswer("CRFOP", Buff, 250);
  if (err < 0)
    return err;
  
  if (Buff[0] >= '0' && Buff[0] <= '9')
    *pPower = (eRfPower) (Buff[0] - '0');
  if (Buff[0] >= 'A' && Buff[0] <= 'C')
    *pPower = (eRfPower)(Buff[0] - 'A' + 10);
  if (Buff[0] >= 'a' && Buff[0] <= 'c')
    *pPower = (eRfPower) (Buff[0] - 'a' + 10);

  return err;
}
//==============================================================================

 
//==============================================================================
// ������� ������ �������� �����-�����������. ��� ���������� �������� ����� �����.
//==============================================================================
int8_t Reyax_bt_SetRfPower(eRfPower Power)
{
  WakeUpIfNeeded();

  char Buff[3];
  DecToStr((uint8_t) Power, Buff);

  uint8_t intPower = (uint8_t) Power;
  
  if (intPower < 10)    // 0...9
    DecToStr(intPower, Buff);
  else                  // A...C
  {
    Buff[0] = intPower - 10 + 'A';
    Buff[1] = 0;
  }  
  
  SendStr("AT+CRFOP=");
  SendStr(Buff);
  SendStr("\r\n");
  
  return RecvAnswer("OK", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ ����� ���������� �������� �����������
//==============================================================================
int8_t Reyax_bt_GetAllowInputConnections(uint8_t *pEnable)
{
  WakeUpIfNeeded();

  SendStr("AT+CNE?\r\n");       // ����� ����: +CNE=0
  
  char Buff[3];
  int8_t err = RecvAnswer("CNE", Buff, 250);
  *pEnable = atoi(Buff);
  
  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ ����� ���������� �������� �����������. ��� ���������� �������� ����� �����.
//==============================================================================
int8_t Reyax_bt_SetAllowInputConnections(uint8_t Enable)
{
  WakeUpIfNeeded();

  char Buff[3];

  DecToStr(Enable ? 1 : 0, Buff);
  
  SendStr("AT+CNE=");
  SendStr(Buff);
  SendStr("\r\n");
  
  return RecvAnswer("RESET!", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ ������� ����������������� �������
//==============================================================================
int8_t Reyax_bt_GetBroadcastPeriod(eBroadcastPeriod *pPeriod)
{
  WakeUpIfNeeded();

  SendStr("AT+PERIOD?\r\n");       // ����� ����: +PERIOD=4
  
  char Buff[3];
  int8_t err = RecvAnswer("PERIOD", Buff, 250);
  *pPeriod = (eBroadcastPeriod) atoi(Buff);
  
  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ ������� ����������������� �������. ��� ���������� �������� ����� �����.
//==============================================================================
int8_t Reyax_bt_SetBroadcastPeriod(eBroadcastPeriod Period)
{
  WakeUpIfNeeded();

  char Buff[3];

  DecToStr(Period, Buff);
  
  SendStr("AT+PERIOD=");
  SendStr(Buff);
  SendStr("\r\n");
  
  return RecvAnswer("OK", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ ������ ����������������
//==============================================================================
int8_t Reyax_bt_GetPowerMode(ePowerMode *pPowerMode)
{
  WakeUpIfNeeded();

  SendStr("AT+PWMODE?\r\n");       // ����� ����: +PWMODE=3
  
  char Buff[3];
  int8_t err = RecvAnswer("PWMODE", Buff, 250);
  *pPowerMode = (ePowerMode) atoi(Buff);
  
  return err;
}
//==============================================================================


//==============================================================================
// ������� ��������� ������ ����������������
//==============================================================================
int8_t Reyax_bt_SetPowerMode(ePowerMode powerMode, uint16_t broadcastOnTime, uint16_t broadcastOffTime)
{
  WakeUpIfNeeded();

  char Buff[5];
  uint8_t intPowerMode = (uint8_t) powerMode;

  if (intPowerMode > 3)
    return REYAX_ERR_WRONG_PARAM;
  if (powerMode == powmode_CustomSave && (broadcastOnTime > 600 || broadcastOffTime > 600))
    return REYAX_ERR_WRONG_PARAM;
  
  SendStr("AT+PWMODE=");
  DecToStr(intPowerMode, Buff);
  SendStr(Buff);
  
  if (powerMode == powmode_CustomSave)
  {
    SendStr(",");
    DecToStr(broadcastOnTime, Buff);
    SendStr(Buff);
    SendStr(",");
    DecToStr(broadcastOffTime, Buff);
    SendStr(Buff);
  }
  
  SendStr("\r\n");
  
  return RecvAnswer("OK", 0, 100);
}
//==============================================================================

 
//==============================================================================
// ������� ������ ����� ������ �������� ����������������� �������
//==============================================================================
int8_t Reyax_bt_GetBroadcastState(uint8_t *pEnable)
{
  WakeUpIfNeeded();

  SendStr("AT+CFUN?\r\n");       // ����� ����: +CFUN=0
  
  char Buff[3];
  int8_t err = RecvAnswer("CFUN", Buff, 250);
  *pEnable = atoi(Buff);
  
  return err;
}
//==============================================================================


//==============================================================================
// ������� ���������� ������ ������ �������� ����������������� �������
//==============================================================================
int8_t Reyax_bt_SetBroadcastState(uint8_t enable)
{
  WakeUpIfNeeded();

  char Buff[3];

  SendStr("AT+CFUN=");
  DecToStr(enable ? 1 : 0, Buff);
  SendStr(Buff);
  SendStr("\r\n");
  
  return RecvAnswer("OK", 0, 100);
}
//==============================================================================


//==============================================================================
// ������� ������ �������� ������ UART
//==============================================================================
int8_t Reyax_bt_GetBaudRate(eBaud *pBaud)
{
  WakeUpIfNeeded();

  SendStr("AT+IPR?\r\n");       // ����� ����: +IPR=7
  
  char Buff[3];
  int8_t err = RecvAnswer("IPR", Buff, 250);
  *pBaud = (eBaud) atoi(Buff);
  
  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ �������� ������ UART (9600/19200/38400/57600/115200). ��� ���������� �������� ����� �����.
//==============================================================================
int8_t Reyax_bt_SetBaudRate(eBaud baud)
{
  WakeUpIfNeeded();

  char Buff[3];

  SendStr("AT+IPR=");   // ����� +Please RESET!
  DecToStr((uint8_t) baud, Buff);
  SendStr(Buff);
  SendStr("\r\n");
  
  return RecvAnswer("RESET", 0, 100);
}
//==============================================================================

 
//==============================================================================
// ������� ������ �������� ����������� ������
//==============================================================================
int8_t Reyax_bt_GetConnectionStatus(eConnectionState *pPort1, eConnectionState *pPort2)
{
  WakeUpIfNeeded();

  SendStr("AT+CONNECT?\r\n");       // ����� ����: +CONNECT=Port1,Port2, ��� PortN: 0 - ��� ����������, C - ���������� �����������, H - Host �����������

  char Buff[6];
  char *pBuff = Buff;
  int8_t err = RecvAnswer("CONNECT", Buff, 250);
  
  portState[0] = convertToConnectionStatus(*pBuff); 
  if (pPort1)
    *pPort1 = portState[0];

  char *pPos = strstr(pBuff, ",");
  int16_t Pos = pPos - pBuff;
  pBuff += (Pos + 1);
      
  portState[1] = convertToConnectionStatus(*pBuff);
  if (pPort2)
    *pPort2 = portState[1];
  
  return err;
}
//==============================================================================

eConnectionState Reyax_bt_GetPortState(uint8_t portNum)
{
  if (portNum >= 1 && portNum <= PORT_NUM)
    return portState[portNum - 1];
  
  return no_connection;
}

//==============================================================================
// ������� ������������ ��������� ���������� (�� 5 ���������)
//==============================================================================
int8_t Reyax_bt_Scan(eNodeInfo *pNodesArray, uint8_t *pDevicesNum)
{
  WakeUpIfNeeded();

  SendStr("AT+SCAN\r\n");       // ����� ����: +.....\r\n ����� +1:0xMAC,BroadcastName,- 36dBm\r\n +Found 1
  
  char Buff[49];
  char *pPos1, *pPos2;
  uint8_t nodeIdx = 0;

  int8_t err = RecvAnswer(".....", Buff, 250);  // ������ ���������� ��������� ������� ������������?
  if (err != 0)
    return REYAX_ERR_NO_ANSVER;
  
  while (1)
  {
    err = RecvLine(Buff, 8000);
    if (err != 0)
      break;
    
    pPos1 = strstr(Buff, "Found");      // ������� ���-�� ������������ �������
    if (pPos1 != 0)
    {
      pPos1 += 5;
      *pDevicesNum = atoi(pPos1);
      break;
    }
    
    // ������ ���������� �� ��������� ������������ ������
    pPos1 = strstr(Buff, ":0x");        // ������� ������ � ����������� �� ��������� ������������ ������
    if (pPos1 != 0)
    {
      pPos1 += 3;
      for (uint8_t byteIdx = 0; byteIdx < 6; byteIdx++)
      {
        // ��������� MAC-�����
        uint8_t byte;
        HexStrToByte(&byte, pPos1);
        pNodesArray[nodeIdx].MAC[byteIdx] = byte;
        pPos1 += 2;
      }
    
      // ��������� Broadcast-���
      pPos1 = strstr(pPos1, ",");
      if (pPos1 == 0)
        return REYAX_ERR_NO_ANSVER;

      pPos1++;
      pPos2 = strstr(pPos1, ",");
      if (pPos2 == 0)
        return REYAX_ERR_NO_ANSVER;
    
      memcpy(pNodesArray[nodeIdx].Name, pPos1, pPos2 - pPos1);
      pNodesArray[nodeIdx].Name[pPos2 - pPos1] = 0;
      pPos1 = pPos2 + 1;
    
      // ��������� RSSI
      pPos2 = strstr(pPos1, "dBm");
      if (pPos2 != 0)
      {
        int8_t rssi = 1;
        if (*pPos1 == '-')
        {
          rssi = -1;
          pPos1++;
        }
        rssi *= atoi(pPos1);
        pNodesArray[nodeIdx].Rssi = rssi;
      }
    
      nodeIdx++;
      if (nodeIdx > REYAX_BT_SCAN_MAX_DEVICES)
        break;
    }
  }
  
  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ MAC-������ �������� ������
//==============================================================================
int8_t Reyax_bt_GetMyMAC(char *pMac)
{
  WakeUpIfNeeded();

  SendStr("AT+ADDR?\r\n");  // ����� ����: +ADDR=123456ABCDEF
  
  char Buff[14];
  int8_t err = RecvAnswer("ADDR", Buff, 250);
  if (!err)
  {
      for (uint8_t byteIdx = 0; byteIdx < 6; byteIdx++)
      {
        // ��������� MAC-�����
        uint8_t byte;
        HexStrToByte(&byte, &Buff[byteIdx << 1]);
        pMac[byteIdx] = byte;
      }
  }

  return err;
}
//==============================================================================

  
//==============================================================================
// ������� ������������ ���������� � ������ ������� RYB080
//==============================================================================
int8_t Reyax_bt_Connect(uint8_t deviceNum)
{
  WakeUpIfNeeded();

  SendStr("AT+CONT=");
  
  char Buff[5];
  DecToStr(deviceNum, Buff);
  SendStr(Buff);
  SendStr("\r\n");
  
  delay_ms(5);

  return 0;
}
//==============================================================================


//==============================================================================
// ������� ������� ���������� � ������ ������� RYB080
//==============================================================================
int8_t Reyax_bt_Disconnect(uint8_t portNum)
{
  if (portNum > PORT_NUM)
    return REYAX_ERR_WRONG_PARAM;

  WakeUpIfNeeded();

  SendStr("AT+DCON=");
  
  char Buff[5];
  DecToStr(portNum, Buff);
  SendStr(Buff);
  SendStr("\r\n");
  
  delay_ms(10);

  return 0;
}
//==============================================================================


//==============================================================================
// ������� �������� ������� �� �������� ������. 
// ����� ������������ portNum = 0 ���� ������������ ������ 1 ����
//==============================================================================
int8_t Reyax_bt_Send(uint8_t portNum, char *pData)
{
  if (portNum > PORT_NUM)
    return REYAX_ERR_WRONG_PARAM;

  uint16_t len = strlen(pData);
  uint8_t maxPaketLen = portNum ? MTU[portNum - 1] - 2 : 62;
  if (len > maxPaketLen)
    return REYAX_ERR_WRONG_PARAM;

  WakeUpIfNeeded();

  char Buff[2];

  // ����
  if (portNum)
  {
    DecToStr(portNum, Buff);
    SendStr(Buff);
    SendStr(">");
  }
  
  // ���������
  UART_Send((uint8_t *)pData, len);

  return RecvAnswer("OK", 0, 500);
}
//==============================================================================


//==============================================================================
// ��������� �������� ���������������� ������
//==============================================================================
int8_t Reyax_bt_printf(uint8_t portNum, const char *args, ...)
{
  va_list ap;
  va_start(ap, args);
  char len = vsnprintf(StrBuff, sizeof(StrBuff), args, ap);
  va_end(ap);

  return Reyax_bt_Send(portNum, StrBuff);
}
//==============================================================================


//==============================================================================
// ������������� ���������� � �������
//==============================================================================
void Reyax_bt_Init(USART_TypeDef* USARTx, uint32_t BaudRate, LoraEventFunc Func, uint8_t NeedWakeUp)
{
  EventFunc = Func;
  UART_Init(USARTx, BaudRate, RxPacket);
  
  Module_State.NeedWakeUp = NeedWakeUp ? 1 : 0;
}
//==============================================================================
