//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _REYAX_LORA_H
#define _REYAX_LORA_H

#include <types.h>

#define REYAX_BT_SCAN_MAX_DEVICES       5       // ������������ ���-�� ���������, �������������� ��� ������������

// ������������ ���� ������
#define REYAX_ERR_NO_ANSVER             (-3)    // ��� ������ - ���� ����-��� �������������� �� �������
#define REYAX_ERR_NO_RX_BUFF            (-4)    // ��� ������ - �� ������� ��������� �� ������� �����
#define REYAX_ERR_WRONG_PARAM           (-5)    // ��� ������ - �� ���������� �������� ��������� �������

typedef enum
{
  pow_m21dBm = 0,       // -21 dBm
  pow_m18dBm = 1,       // -18 dBm
  pow_m15dBm = 2,       // -15 dBm
  pow_m12dBm = 3,       // -12 dBm
  pow_m9dBm = 4,        // -9 dBm
  pow_m6dBm = 5,        // -6 dBm
  pow_m3dBm = 6,        // -3 dBm
  pow_0dBm = 7,         // 0 dBm
  pow_1dBm = 8,         // 1 dBm
  pow_2dBm = 9,         // 2 dBm
  pow_3dBm = 10,        // 3 dBm
  pow_4dBm = 11,        // 4 dBm
  pow_5dBm = 12         // 5 dBm
} eRfPower; 

typedef enum
{
  period_25ms = 0,      // 25 ms
  period_50ms = 1,      // 50 ms
  period_100ms = 2,     // 100 ms
  period_200ms = 3,     // 200 ms
  period_500ms = 4,     // 500 ms
  period_1sec = 5,      // 1 sec
  period_2sec = 6,      // 2 sec
  peripd_3sec = 7,      // 3 sec
  period_5sec = 8,      // 5 sec
  period_10sec = 9      // 10 sec
} eBroadcastPeriod; 

typedef enum
{
  powmode_FullFunction = 0,     // Broadcast ON, UART ON                                                // Fully Function Mode: UART and BLE broadcasting is in operation. 
  powmode_PowerSave = 1,        // Broadcast ON, UART ����������� ����� 5 ��� ���������� ����������     // Standard Saving Power Mode: After pin4 was triggered, UART can send the AT Command. If pin4 was not triggered again in 5 seconds, it will turn into it will turn into only broadcasting mode. (default)  
  powmode_Sleep = 2,            // Broadcast OFF, UART ����������� ����� 5 ��� ���������� ����������    // Sleep Mode: Under the Standard Saving Power Mode turning off the BLE broadcasting function. 
  powmode_CustomSave = 3        // ��� PowerSave, �� ����� ON/OFF broadcast ������ �������              // Customized Saving Power Mode: Customized the setting of the turn-on and turn-off cycle time of Bluetooth broadcasting.  
} ePowerMode;


typedef enum
{
  baud9600 = 4,
  baud19200 = 5,
  baud38400 = 6,
  baud57600 = 7,
  baud115200 = 8
} eBaud;

typedef enum
{
  no_connection,
  client_connection,
  host_connection
} eConnectionState;

typedef struct
{
  uint8_t MAC[6];
  char Name[20];
  int8_t Rssi;
} eNodeInfo;

    
// ��� ��������� �� ������� ��������� ��������� ������
typedef void (*LoraEventFunc)(uint8_t portNum, char *pStr);


// ������� ������ �������� ������ UART ������
int8_t Reyax_Lora_SetBaudRate(uint32_t Baud);
// ������� ������ �������� ������ UART ������
int8_t Reyax_Lora_GetBaudRate(uint32_t *pBaud);
// ������� �������� ������� �� �������� ������
int8_t Reyax_Lora_Send(uint16_t Address, char *pData);
// ������� ������ ���������� ������
int8_t Reyax_Lora_SetParameters(uint8_t SpreadingFactor, uint8_t Bandwidth, uint8_t CodingRate, uint8_t Preamble);
// ������� ������ ���������� ������
int8_t Reyax_Lora_GetParameters(uint8_t *pSpreadingFactor, uint8_t *pBandwidth, uint8_t *pCodingRate, uint8_t *pPreamble);
// ������� ������ 32-����������� ����� ���� (AES128) (�� ������ � 16-���� ����)
int8_t Reyax_Lora_SetPasswordStr(char *pPassword);
// ������� ������ 16-�������� ����� ���� (AES128) (�� ������� ����)
int8_t Reyax_Lora_SetPasswordBin(uint8_t *pPassword);
// ������� ������ 32-����������� ����� ���� (AES128) (� ������ � 16-���� ����)
int8_t Reyax_Lora_GetPasswordStr(char *pPassword);
// ������� ������ 16-�������� ����� ���� (AES128) (� ������ ����)
int8_t Reyax_Lora_GetPasswordBin(uint8_t *pPassword);
// ������� ������ �������� �������� (0-15)
int8_t Reyax_Lora_SetPower(uint8_t Power);
// ������� ������ �������� �������� (0-15)
int8_t Reyax_Lora_GetPower(uint8_t *pPower);
// ������� ������ ����� ������ ���
int8_t Reyax_Lora_SetSleepMode(uint8_t SleepModeOn);
// ������� ������ ����� ������ ���
int8_t Reyax_Lora_GetSleepMode(uint8_t *pSleepModeOn);
// ������� ������ ����������� ������� ������ ������
int8_t Reyax_Lora_SetBand(uint32_t Band);
// ������� ������ ����������� ������� ������ ������
int8_t Reyax_Lora_GetBand(uint32_t *pBand);
// ������� ������ ������ ������ (��������� �� �����)
int8_t Reyax_Lora_SetAddress(uint16_t Address);
// ������� ������ ������ ������ (��������� �� �����)
int8_t Reyax_Lora_GetAddress(uint16_t *pAddress);
// ������� ������ ID ����
int8_t Reyax_Lora_SetNetworkID(uint8_t Id);
// ������� ������ ID ����
int8_t Reyax_Lora_GetNetworkID(uint8_t *pId);

// ������������� ���������� � �������
void Reyax_bt_Init(USART_TypeDef* USARTx, uint32_t BaudRate, LoraEventFunc Func, uint8_t NeedWakeUp);
// ������� �������� ������� �������� ����� �� UART � �������
int8_t Reyax_bt_IsPresent();
// ������� �������� ������� ������ ������
int8_t Reyax_bt_Reset();
// ������� ������ ������ �������� ������
int8_t Reyax_bt_GetVersion(char *pVersion);
// ������� ������ ������������������ ����� (�� 20 ��������)
int8_t Reyax_bt_GetBroadcastName(char *pName);
// ������� ������ ������������������ ����� (�� 20 ��������). ��� ���������� �������� ����� �����.
int8_t Reyax_bt_SetBroadcastName(char *pName);
// ������� ������ ����� ���������� (������������ � ���������� ��� iOS)
int8_t Reyax_bt_GetDeviceName(char *pName);
// ������� ������ ����� ���������� (�� 20 ��������, ������������ � ���������� ��� iOS). ��� ���������� �������� ����� �����.
int8_t Reyax_bt_SetDeviceName(char *pName);
// ������� ������ �������� �����-�����������
int8_t Reyax_bt_GetRfPower(eRfPower *pPower);
// ������� ������ �������� �����-�����������. ��� ���������� �������� ����� �����.
int8_t Reyax_bt_SetRfPower(eRfPower Power);
// ������� ������ ����� ���������� �������� �����������
int8_t Reyax_bt_GetAllowInputConnections(uint8_t *pEnable);
// ������� ������ ����� ���������� �������� �����������. ��� ���������� �������� ����� �����.
int8_t Reyax_bt_SetAllowInputConnections(uint8_t Enable);
// ������� ������ ������� ����������������� �������
int8_t Reyax_bt_GetBroadcastPeriod(eBroadcastPeriod *pPeriod);
// ������� ������ ������� ����������������� �������. ��� ���������� �������� ����� �����.
int8_t Reyax_bt_SetBroadcastPeriod(eBroadcastPeriod Period);
// ������� ������ ������ ����������������
int8_t Reyax_bt_GetPowerMode(ePowerMode *pPowerMode);
// ������� ������ ������ ����������������. ��� ���������� �������� ����� �����.
int8_t Reyax_bt_SetPowerMode(ePowerMode powerMode, uint16_t broadcastOnTime, uint16_t broadcastOffTime);
// ������� ������ ����� ������ �������� ����������������� �������
int8_t Reyax_bt_GetBroadcastState(uint8_t *pEnable);
// ������� ���������� ������ ������ �������� ����������������� �������
int8_t Reyax_bt_SetBroadcastState(uint8_t enable);
// ������� ������ �������� ������ UART
int8_t Reyax_bt_GetBaudRate(eBaud *pBaud);
// ������� ������ �������� ������ UART (9600/19200/38400/57600/115200). ��� ���������� �������� ����� �����.
int8_t Reyax_bt_SetBaudRate(eBaud baud);
// ������� ������ �������� ����������� ������
int8_t Reyax_bt_GetConnectionStatus(eConnectionState *pPort1, eConnectionState *pPort2);

// ������� �������� ������� ��������� ��������� ����� GPIO
int8_t Reyax_bt_GPIO_set(uint8_t pin_num, uint8_t state);
// ������� ������ MAC-������ �������� ������
int8_t Reyax_bt_GetMyMAC(char *pMac);

#endif