//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x.h>
#include <string.h>
#include <stdio.h>
#include <ryb080.h>
#include <hd44780.h>
#include <gpio.h>
#include <uart.h>
#include <timers.h>
#include <delay.h>
#include "main.h"


#define RxMode          1       // 1 - ��� �������� � ��������, 0 - ��� ����������� ��� �������


char StringBuffer[100];
static uint32_t Rx_Cnt = 0;
static uint32_t Rx_Size = 0;

// ������� ���������� �����������
#define LedOff()        GPIO_WriteBit(GPIOB, (1 << 12), Bit_SET)
#define LedOn()         GPIO_WriteBit(GPIOB, 1 << 12, Bit_RESET)

uint16_t RxLen;

//==============================================================================
// ���������-���������� ������� �� �������. pBuff - ����� � �������� �� ������� 
//==============================================================================
void Bt_Rx(uint8_t portNum, char *pStr)
{
  uint16_t rxlen = strlen(pStr);
  if (rxlen > sizeof(StringBuffer))
  {
    // ��������� ������ �������������� ����� � ������� ������
    return;
  }
  strcpy(StringBuffer, pStr);
  Rx_Cnt++;
  Rx_Size += rxlen;
}
//==============================================================================

int8_t err = 0;
char buff[32];
char RxNodeName[] = "REYAX_RX_NODE";
uint8_t NotificationMode;

eBaud baud = baud9600;
eRfPower power = pow_5dBm;
eBroadcastPeriod period = period_100ms;
ePowerMode powerMode = powmode_FullFunction;
eConnectionState portState1, portState2;
eNodeInfo nodes[REYAX_BT_SCAN_MAX_DEVICES];

//==============================================================================
//
//==============================================================================
void main()
{
  uint8_t value;
  uint32_t pakCnt = 0;

  SystemInit();

#if (RxMode)
  hd44780_init();
  hd44780_backlight_set(1);
#else
  gpio_PortClockStart(GPIOA);
  gpio_SetGPIOmode_In(GPIOA, 1 << 15, gpio_PullUp);
#endif    

  gpio_PortClockStart(GPIOB);
  gpio_SetGPIOmode_Out(GPIOB, 1 << 12);

  Reyax_bt_Init(USART1, 115200, Bt_Rx, 1);      // �� ��������� �������� 9600
  delay_ms(200);

  // ��������� �������� �����������
  err = Reyax_bt_SetRfPower(pow_5dBm);
  // ������ �������� �����������
  err = Reyax_bt_GetRfPower(&power);
/*
  // ��������� ����� ���������� �������� �����������
  err = Reyax_bt_SetAllowInputConnections(1);
  // ������ ����� ���������� �������� �����������
  err = Reyax_bt_GetAllowInputConnections(&value);
  // ��������� ������� ����������������� �������
  err = Reyax_bt_SetBroadcastPeriod(period);
  // ������ ������� ����������������� �������
  err = Reyax_bt_GetBroadcastPeriod(&period);
  // ��������� ������ ����������������
  powerMode = powmode_FullFunction;// powmode_PowerSave;
  err = Reyax_bt_SetPowerMode(powerMode, 60, 60);
  // ������ ������ ����������������
  err = Reyax_bt_GetPowerMode(&powerMode);
  // ������ ����� ������ �������� ����������������� �������
  err = Reyax_bt_SetBroadcastState(1);
  // ������ ����� ������ �������� ����������������� �������
  err = Reyax_bt_GetBroadcastState(&value);
  // ��������� ��������� ����� GPIO
  err = Reyax_bt_GPIO_set(7, 1);
*/

#if (RxMode)
  hd44780_clear();

  // ��������� ������������������ ����� ������
  err = Reyax_bt_SetBroadcastName(RxNodeName);    // �� ��������� - REYAX_BLE_RYB080I 

  // ������ ������������������ ����� ������
  err = Reyax_bt_GetBroadcastName(buff);
  hd44780_goto_xy(0, 0);
  if (!err)
    hd44780_printf("%s", buff);

  // ������ MAC-������ ������
  err = Reyax_bt_GetMyMAC(buff);
  hd44780_goto_xy(1, 0);
  if (!err)
    hd44780_printf("MAC: %X%X%X%X%X%X", buff[0], buff[1], buff[2], buff[3], buff[4], buff[5]);

  delay_ms(2000);
  hd44780_clear();
#else
  if (GPIO_ReadInputDataBit(GPIOA, (1 << 15)))
    NotificationMode = 0;
  else
    NotificationMode = 1;

  Reyax_bt_GetConnectionStatus(0, 0);
  if (Reyax_bt_GetPortState(1) != no_connection)
    Reyax_bt_Disconnect(1);
  if (Reyax_bt_GetPortState(2) != no_connection)
    Reyax_bt_Disconnect(2);

  delay_ms(300);

  uint8_t nodes_num = 0;
  err = Reyax_bt_Scan(nodes, &nodes_num);
    
  for (uint8_t node = 0; node < nodes_num; node++)
  {
    if (strstr(nodes[node].Name, RxNodeName))
    {
      Reyax_bt_Connect(node + 1);
      break;
    }
  }

  delay_ms(2000);
  delay_ms(500);
  
#endif
  
  while (1)
  {
#if (RxMode)
    // ����� �� �������
    hd44780_goto_xy(0, 0);
    hd44780_printf("RX:%5d %d ", Rx_Cnt, Rx_Size);

    hd44780_goto_xy(1, 0);
    char tmpBuff[17];
    memcpy(tmpBuff, StringBuffer, 16);
    tmpBuff[16] = 0;
    hd44780_printf("%s ", tmpBuff);
    delay_ms(100);
#else
  pakCnt++;

  if (NotificationMode)
  {
    LedOn();
    Reyax_bt_printf(0, "TX:%5d message012345678901234567890123456789012345678901\r\n", pakCnt);
    LedOff();
    delay_ms(70);
  }
  else
  {
    LedOn();

    if (Reyax_bt_GetPortState(1) != no_connection)
      Reyax_bt_printf(1, "TX:%5d message012345678901234567890123456789012345678901\r\n", pakCnt);
    if (Reyax_bt_GetPortState(2) != no_connection)
      Reyax_bt_printf(2, "TX:%5d message012345678901234567890123456789012345678901\r\n", pakCnt);

    LedOff();

    //delay_ms(500);        // 12% ������
    //delay_ms(600);        // 5% ������
    delay_ms(700);        // 0.4% / 0.2% ������
    //delay_ms(800);        // 0.4% ������?
    //delay_ms(900);        // 1.2% / 0.024% ������?
    //delay_ms(1000);       // 0.28% ������?
  }
#endif
  }
}
//==============================================================================
